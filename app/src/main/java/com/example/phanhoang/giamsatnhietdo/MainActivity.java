package com.example.phanhoang.giamsatnhietdo;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    public static final int TEMP_NORMAL = 27;
    public static final int TEMP_WARNING = 29;

    private DatabaseReference fireBase;
    TextView txtTemp;
    TextView txtLevel;
    Button btnOnAlert;
    Button btnOffAlert;
    TextView txtStatus;
    boolean offAlert = false; // giá trị kiểm tra tắt cảnh báo
    NotificationCompat.Builder builder;// tao ra mot thong bao

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtTemp = (TextView) findViewById(R.id.txtTemp);
        txtLevel = (TextView) findViewById(R.id.txtLevel);
        btnOffAlert = (Button) findViewById(R.id.btnOffAlert);
        btnOnAlert = (Button) findViewById(R.id.btnOnAlert);
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true);
        control();
        addButton();
    }

    private void control() {
        fireBase = FirebaseDatabase.getInstance().getReference().getRoot();//lay ra du lieu data từ firebase rồi gán vào temp
        fireBase.child("offAlert").setValue(false); // khi khởi động ứng dụng thì đặt cảnh báo mặc định
        fireBase.child("temperature").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                float temp = Float.parseFloat(dataSnapshot.getValue().toString());
                txtTemp.setText(temp + " độ C");
                if (temp < TEMP_NORMAL) {
                    txtLevel.setText("Bình thường");
                    txtStatus.setText("Cảnh báo: TẮT");
                    // nhiệt độ < 33 độ thì không có đèn
                } else if (temp < TEMP_WARNING) {
                    txtLevel.setText("Cần lưu ý");
                    txtLevel.setTextColor(Color.GREEN);
                    if (offAlert == true) {
                        fireBase.child("offAlert").setValue(true);
                        txtStatus.setText("Cảnh báo: TẮT");
                        // nhiệt độ > 33 độ và đã ấn nút tắt cảnh báo thì không có đèn
                    } else {
                        fireBase.child("offAlert").setValue(false);
                        txtStatus.setText("Cảnh báo: BẬT");
                        // nhiệt độ > 33 độ và chưa ấn nút tắt cảnh báo thì đèn sáng
                        sendNoti("NGUY HIỂM",String.format("Nhiệt độ đang ở mức trên %1$s độ C!!!",TEMP_NORMAL));
                    }
                } else {
                    txtLevel.setText("NGUY HIỂM");
                    txtLevel.setTextColor(Color.RED);
                    if (offAlert == true) {
                        fireBase.child("offAlert").setValue(true);
                        txtStatus.setText("Cảnh báo: TẮT");
                        // nhiệt độ > 35 độ và đã ấn nút tắt cảnh báo thì không có đèn
                    } else {
                        fireBase.child("offAlert").setValue(false);
                        txtStatus.setText("Cảnh báo: BẬT");
                        //    builder.setContentText("NGUY HIỂM: Nhiệt độ đang ở mức trên 33 độ!");
                        // nhiệt độ > 35 độ và chưa ấn nút tắt cảnh báo thì đèn sáng
                        sendNoti("NGUY HIỂM",String.format("Nhiệt độ đang ở mức trên %1$s độ C!!!",TEMP_WARNING));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addButton() {
        // ấn nút tắt cảnh báo
        btnOffAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offAlert = true; // giá trị kiểm tra tắt thông báo
                txtStatus.setText("Cảnh báo: TẮT");
                fireBase.child("offAlert").setValue(true); // đổi giá trị trường "offAlert" trên firebase thành true
                btnOnAlert.setEnabled(true); // nút đặt lại sẽ ấn được
            }
        });
        btnOnAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offAlert = false; // giá trị kiểm tra tắt thông báo
                fireBase.child("offAlert").setValue(false); // đổi giá trị trường "offAlert" trên firebase thành false
                btnOnAlert.setEnabled(false); // nút đặt lại sẽ không ấn được nữa
            }
        });
    }

    private void sendNoti(String title, String content) {
        Intent intent = new Intent(this, MainActivity.class);//mo activity khac nx khong kiem soat gtri tra ve
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setSmallIcon(R.drawable.attention);// bieu tuong thong bao
        builder.setContentTitle(title);// tieu de thong bao
        builder.setContentText(content);// chi tiet van ban
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(001, builder.build());

    }
}
